//first NodeJS application
const express = require('express')
const app = express()
const port = 3000
app.get('/', (req, res) => {
    res.send('Hello Virtual Machine!')
})
app.get('/servicea', (req, res) => {
    res.send('Hello from service a!')
})
app.get('/serviceb', (req, res) => {
    res.send('Hello from service b!')
})
app.listen(port, () => {
    console.log(`Express Application listening at port 3000`)
})
